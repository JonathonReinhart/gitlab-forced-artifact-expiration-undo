#!/bin/bash
# Undo painful forced artifact expiration migration done by gitlab updates.
# https://gitlab.com/groups/gitlab-org/-/epics/7097

set -e

DB="gitlabhq_production"

function sql_query() {
	# Query is read from stdin
	sudo -i -u gitlab-psql /opt/gitlab/embedded/bin/psql \
		-h /var/opt/gitlab/postgresql \
		--dbname="$DB"
}


function do_inspect() {
	echo "Artifacts expiring at midnight local time on the 22nd of any month:"
	echo ""

	sql_query <<- END_SQL
	select count(*), (expire_at at time zone '${TIMEZONE}') as expire_at_localtime
		from ci_job_artifacts where ${EXPIRE_MIDNIGHT_22ND}
	group by expire_at;
	END_SQL
}

function do_dump() {
	csv_path="/tmp/ci_job_artifacts_expiring_midnight_edt_on_22nd.csv"

	sql_query <<- END_SQL
	copy (
		select * from ci_job_artifacts where ${EXPIRE_MIDNIGHT_22ND}
	) to '${csv_path}' csv header;
	END_SQL

	echo ""
	echo "Rows written to $csv_path"
}

function do_fix() {
	do_dump

	echo ""
	echo "Unsetting artifact expiration:"

	sql_query <<- END_SQL
	update ci_job_artifacts set expire_at = null where ${EXPIRE_MIDNIGHT_22ND};
	END_SQL
}

function usage() {
	echo "Usage: $(basename $0) {inspect,dump,fix}"
	echo ""
	echo "This script undoes forced GitLab artifact expiration migrations."
	echo ""
	echo "Actions:"
	echo "  inspect     Inspect current forced-expiring artifacts"
	echo "  dump        Dump forced-expiring artifact rows to csv file"
	echo "  fix         Fix the problem by setting expire_at dates to null"
	echo ""
}

if [ $# -ne 1 ]; then
	usage
	exit 1
fi
action="$1"

# Detect timezone
if [[ -z "$TIMEZONE" ]] && command -v timedatectl >/dev/null; then
    TIMEZONE=$(timedatectl 2>/dev/null | grep 'Time zone' | awk '{ print $3 }')
    #echo "Detected TIMEZONE=\"$TIMEZONE\" via timedatectl"
fi
if [[ -z "$TIMEZONE" && -f /etc/timezone ]]; then
    TIMEZONE=$(cat /etc/timezone)
    #echo "Detected TIMEZONE=\"$TIMEZONE\" via /etc/timezone"
fi
if [[ -z "$TIMEZONE" ]]; then
    echo "ERROR: Failed to detect timezone." >&2
    echo "Run with 'TIMEZONE=... $0'" >&2
    exit 1
fi
echo "Using TIMEZONE=\"$TIMEZONE\"" >&2

EXPIRE_MIDNIGHT_22ND="extract(day from expire_at) = 22"
EXPIRE_MIDNIGHT_22ND+=" and (expire_at at time zone '${TIMEZONE}')::time = '00:00:00'"

# Run action
case $action in
inspect)
	do_inspect
	;;
dump)
	do_dump
	;;
fix)
	do_fix
	;;
*)
	echo "ERROR: Unknown action \"$action\""
	usage
	exit 1
	;;
esac
