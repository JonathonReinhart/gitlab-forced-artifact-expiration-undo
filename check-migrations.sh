#!/bin/bash
# Check for problematic migrations which may lead to data loss.
# https://gitlab.com/groups/gitlab-org/-/epics/7097

DB="gitlabhq_production"

function sql_query() {
	# Query is read from stdin
	sudo -i -u gitlab-psql /opt/gitlab/embedded/bin/psql \
		-t -A \
		-h /var/opt/gitlab/postgresql \
		--dbname="$DB"
}

function check_migration() {
	version="$1"
	classname="$2"
	output=$(sql_query <<- END_SQL
	select * from schema_migrations where version='${version}';
	END_SQL
	)

	if [[ -n $output ]]; then
		printf  "\e[31m%16s %-48s %s\e[m\n" ${version} ${classname} "HAS RUN!"
		return 1
	else
		printf  "\e[32m%16s %-48s %s\e[m\n" ${version} ${classname} "has not run."
		return 0
	fi
}

function check_migrations() {
	check_migration 20201208175117 ScheduleBackfillingArtifactExpiryMigration
	check_migration 20210115220610 ScheduleArtifactExpiryBackfill
	check_migration 20210224150506 RescheduleArtifactExpiryBackfill
	check_migration 20210413132500 RescheduleArtifactExpiryBackfillAgain
}

echo "Checking migrations..."
check_migrations
