GitLab artifact forced expiration fixer
=======================================

See: https://gitlab.com/groups/gitlab-org/-/epics/7097

# Disclaimer
This script directly modifies the GitLab database.
Use at your own risk only after backing up your GitLab instance!
The author cannot be held liable for any loss of data caused as a result of
running this script.

# Usage
This script undoes forced GitLab artifact expiration migrations.

```
Usage: undo-forced-expiration.sh {inspect,dump,fix}
```

Actions:
- **`inspect`** - Inspect current forced-expiring artifacts
- **`dump`** - Dump forced-expiring artifact rows to CSV file (path is hard-coded in /tmp)
- **`fix`** - Undo the forced expiration by setting `expire_at` dates to null (also does `dump`)

## Example session

Inspect:
```
$ ./undo-forced-expiration.sh inspect
Artifacts expiring at midnight local time on the 22nd of any month:

 count | expire_at_localtime 
-------+---------------------
 67697 | 2022-03-22 00:00:00
(1 row)
```

Fix:
```
$ ./undo-forced-expiration.sh fix
COPY 67697

Rows written to /tmp/ci_job_artifacts_expiring_midnight_edt_on_22nd.csv

Unsetting artifact expiration:
UPDATE 67697
```
